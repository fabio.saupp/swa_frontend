import axios from 'axios';
const BACKEND_APP_URL = "http://localhost:8080/v1";


export const loginUser = (username, password) => {
  
  return axios({
    url: BACKEND_APP_URL + "/users/login",
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({
      "username": username,
      "password": password
    })
  });
}

export const getCustomerData = (value) => {
  var urls = "";
  if(value!==""){
    urls = BACKEND_APP_URL + "/customers?name="+value;
  } else {
    urls = BACKEND_APP_URL + "/customers";
  }

  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const getCustomerByUserIdData = () => {
  var urls = BACKEND_APP_URL + "/customers/user/"+localStorage.getItem('userId');;

  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}



export const addCustomer = (name, department, address) => {
  
  return axios({
    url: BACKEND_APP_URL + "/customers",
    method: "POST",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    data: JSON.stringify({
      "name": name,
      "department": department,
      "address":address
    })
  });
}

export const editCustomer = (id, name, department, address) => {
  
  return axios({
    url: BACKEND_APP_URL + "/customers/"+id,
    method: "PUT",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    data: JSON.stringify({
      "name": name,
      "department": department,
      "address":address
    })
  });
}

export const deleteCustomers = (id) => {
  
  return axios({
    url: BACKEND_APP_URL + "/customers/"+id,
    method: "DELETE",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
  });
}

export const getContractorData = () => {
  var urls = BACKEND_APP_URL + "/contracts";
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const getContractorsByUserId = () => {
  var urls = BACKEND_APP_URL + "/contracts/user/"+localStorage.getItem("userId");
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const getContractorById = (id) => {
  var urls = BACKEND_APP_URL + "/contracts/"+id;
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const deleteContractors = (id) => {
  
  return axios({
    url: BACKEND_APP_URL + "/contracts/"+id,
    method: "DELETE",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
  });
}

export const deleteUserById = (id) => {
  return axios({
    url: BACKEND_APP_URL + "/users/"+id,
    method: "DELETE",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
  });
}

export const getUserById = (id) => {
  var urls = BACKEND_APP_URL + "/users/"+id;
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const addContractor = (sdate, edate, version, selectValue, selectUserValue, ipNumber1, ipNumber2, ipNumber3, featureA, featureB, featureC, licenseKey) => {
  
  return axios({
    url: BACKEND_APP_URL + "/contracts",
    method: "POST",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    data: JSON.stringify({
      "startDate": sdate,
      "customerId": selectValue,
      "endDate": edate,
      "ip1": ipNumber1,
      "ip2": ipNumber2,
      "ip3": ipNumber3,
      "version": version,
      "feature1": featureA,
      "feature2": featureB,
      "feature3": featureC,
      "licenseKey": licenseKey,
      "users":selectUserValue
    })
  });
}

export const editContractor = (id, sdate, edate, version, selectValue, selectUserValue, ipNumber1, ipNumber2, ipNumber3, featureA, featureB, featureC, licenseKey) => {
  
    return axios({
      url: BACKEND_APP_URL + "/contracts/"+id,
      method: "PUT",
      headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
      data: JSON.stringify({
        "startDate": sdate,
        "customerId": selectValue,
        "endDate": edate,
        "ip1": ipNumber1,
        "ip2": ipNumber2,
        "ip3": ipNumber3,
        "version": version,
        "feature1": featureA,
        "feature2": featureB,
        "feature3": featureC,
        "licenseKey": licenseKey,
        "users":selectUserValue
      })
    });
}


export const getUserDataByCustomer = () => {
  var urls = BACKEND_APP_URL + "/users/bycontractor/allusers";
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const getCurrentUserData = () => {
  var urls = BACKEND_APP_URL + "/users/"+localStorage.getItem('userId');
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}


export const getAllUsersByCustomerId = (id) => {
  var urls = BACKEND_APP_URL + "/users/customers/"+id;
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}

export const getAllContractsByCustomerId = (id) => {
  var urls = BACKEND_APP_URL + "/contracts/customers/"+id;
  
  return axios({
    url: urls,
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}


export const getAllUsers = () => {

  return axios({
    url: BACKEND_APP_URL + "/users",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    method: "GET"
  });
}





export const getAllUsersWithUserRole = () => {

  return axios({
    url: BACKEND_APP_URL + "/users_with_user_role",
    headers: { 'Content-Type': 'application/json', 'token': '' + localStorage.getItem('token') },
    method: "GET"
  });
}

export const logoutUser = (userId) => {
  return axios({
    url: BACKEND_APP_URL + "/auth/signout/" + userId,
    method: "POST",
    headers: { 'Content-Type': 'application/json' }
  });
}
export const csvFileUpload = (file) => {
  const formData = new FormData();
  formData.append('file', file)
  return axios({
    url: BACKEND_APP_URL + "/file/upload/" + localStorage.getItem("userId"),
    method: "POST",
    headers: { 'Content-Type': 'multipart/form-data', 'token': '' + localStorage.getItem('token') },
    data: formData
  });
}

export const registerUser = (customerId, fname, lname, email, password, username, phone, mobile, isAdmin) => {
  console.log("register user called" + JSON.stringify({
    "customerId":customerId,
    "firstName": fname,
    "lastName": lname,
    "password": password,
    "email": email,
    "username": username,
    "phone":phone,
    "mobile": mobile,
    "role": isAdmin ? "ADMIN":"USER"
  }));
  return fetch(BACKEND_APP_URL + "/users", {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      "customerId":customerId,
      "firstName": fname,
    "lastName": lname,
    "password": password,
    "email": email,
    "username": username,
    "phone":phone,
    "mobile": mobile,
    "role": isAdmin ? "ADMIN":"USER"
    })
  });
}

export const editUserTobk = (userId, customerId, fname, lname, email, password, username, phone, mobile, isAdmin) => {
  console.log("register user called" + JSON.stringify({
    "customerId":customerId,
    "firstName": fname,
    "lastName": lname,
    "password": password,
    "email": email,
    "username": username,
    "phone":phone,
    "mobile": mobile,
    "role": isAdmin ? "ADMIN":"USER"
  }));
  return fetch(BACKEND_APP_URL + "/users/"+userId, {
    method: "PUT",
    headers: { 'Content-Type': 'application/json', 'Authorization': '' + "Basic " + btoa(localStorage.getItem('username') + ":" + localStorage.getItem('token')) },
    body: JSON.stringify({
      "customerId":customerId,
      "firstName": fname,
    "lastName": lname,
    "password": password,
    "email": email,
    "username": username,
    "phone":phone,
    "mobile": mobile,
    "role": isAdmin ? "ADMIN":"USER"
    })
  });
}

export const readData = () => {

  return axios({
    url: BACKEND_APP_URL + "/file",
    method: "GET",
    headers: { 'Content-Type': 'application/json', 'token': '' + localStorage.getItem('token') },
  });
}

export const updatePassword = (email, password) => {
  console.log("register user called" + JSON.stringify({
    "password": password,
    "email": email
  }));
  return fetch(BACKEND_APP_URL + "/auth/reset-password", {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      "password": password,
      "email": email
    })
  });
}
