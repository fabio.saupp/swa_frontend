import { Checkbox, FormControl, Grid, InputLabel, MenuItem, OutlinedInput, Select, Snackbar, TextField } from '@mui/material';
import * as React from 'react';
import { getCustomerData, registerUser } from '../../services/services';
export default function AddUsers({ toggleModal }) {
  //This js file is mainly to register users and it will take care all validations as well

  const [selectValue, setSelectValue] = React.useState("");

  const [customerData, setCustomerData] = React.useState([]);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [emailError, setEmailError] = React.useState('');
  const [mobileError, setMobileError] = React.useState('');
  const [cpasswordError, setcPasswordError] = React.useState('');
  const [invalidError, setInvalidError] = React.useState('');
  const [snackMessage, setSnackMessage] = React.useState('');


  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [cpassword, setCPassword] = React.useState("");
  const [fname, setFName] = React.useState("");
  const [lname, setLName] = React.useState("");
  const [uname, setUName] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [mobile, setMobile] = React.useState("");
  const [isAdmin, setIsAdmin] = React.useState(false);

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const handleAdminChange = (event) => {
    setIsAdmin(event.target.checked);
  };

  const handleUnameChange = (event) => {
    setUName(event.target.value);
  };

  React.useEffect(() => {
    getCustomerData("").then(resp => {
      console.log(resp.data);
      setCustomerData(resp.data);
    }).catch(error => {
      console.log("login user err ", error);
    });

  }, []);


  const passwordChange = (event) => {
    setPassword(event.target.value);
  }

  const phoneChange = (event) => {
    setPhone(event.target.value);
  }

  const mobileChange = (event) => {
    setMobile(event.target.value);
  }

  const cpasswordChange = (event) => {
    setCPassword(event.target.value);
  }

  const emailChange = (event) => {
    setEmail(event.target.value);
    if (!ValidateEmail(event.target.value)) {
      setEmailError('Enter valid Email!');
    } else {
      setEmailError('');
    }
  }

  const nameFChange = (event) => {
    setFName(event.target.value);
  }

  const nameLChange = (event) => {
    setLName(event.target.value);
  }

  const clickRegister = () => {

    if (email === "" || email === undefined || password === "" || password === undefined ||
      fname === "" || fname === undefined || lname === "" || lname === undefined ||
      phone === "" || phone === undefined || mobile === "" || mobile === undefined ||
      selectValue === "" || selectValue === undefined) {
      setSnackMessage('Please fill out this field');
      setOpenSnack(true);
    } else if (!ValidateEmail(email)) {
      setSnackMessage('Email is not valid!!');
      setOpenSnack(true);
      return false;
    } else if (!phonenumber(mobile)) {
      setSnackMessage('Mobile is not valid!!');
      setOpenSnack(true);
      return false;
    } else if (!phonenumber(phone)) {
      setSnackMessage('Phone is not valid!!');
      setOpenSnack(true);
      return false;
    } else {
      registerUser(selectValue, fname, lname, email, password, uname, phone, mobile, isAdmin).then(res => {
        console.log(res)
        if (res.ok) {
          setFName("");
          setLName("");
          setEmail("");
          setPassword("");
          setSnackMessage('User added successfully!');
          setOpenSnack(true);
          toggleModal();
        } else {
          res.text().then(text => {
            let err = JSON.parse(text);
            console.log(err);

            if (err.error) {
              setcPasswordError(err.errors[0].defaultMessage);
              setSnackMessage(err.errors[0].defaultMessage);
            } else {
              setcPasswordError(err.message);
              setSnackMessage(err.message);
            }
            setOpenSnack(true);
          })
        }

      })
        .catch(error => {
          console.log("Regiter failed" + error);
          setInvalidError('Registration Failed!');
        })
    }
  }

  function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return (true)
    }
    return (false)
  }

  function phonenumber(mobile) {
    var phoneno = /^\d{10}$/;
    if (mobile.match(phoneno)) {
      return true;
    }
    else {
      return false;
    }
  }

  const clickLogin = () => {
    toggleModal();
  }

  const handleChange = (event) => {
    setSelectValue(event.target.value);
  };

  const [logButtonName, setlogButtonName] = React.useState("LOGIN");

  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };

  return (
    <React.Fragment >
      <div className="w-full py-10 px-5 md:px-10">
        <div className="text-center mb-10">
          <h1 className="font-bold text-3xl text-gray-900">Add users</h1>
          <br></br>

          <Grid container direction={"row"}>
            <Grid md={12}>
              <FormControl fullWidth style={{ width: '95%' }}>
                <InputLabel id="demo-multiple-checkbox-label"  >Select customer</InputLabel>
                <Select
                  labelId="demo-multiple-checkbox-label"
                  id="demo-multiple-checkbox"
                  size="large"
                  value={selectValue}
                  onChange={handleChange}
                  input={<OutlinedInput label="Select customer" />}
                // input={<OutlinedInput label="Select services" />}
                // renderValue={(selected) => setSelectValue(selected)}
                // MenuProps={MenuProps}
                >
                  {customerData.map((eachItem) => (
                    <MenuItem key={eachItem.name} value={eachItem.id}>
                      {eachItem.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid></Grid>
          <br></br>
          <Grid container direction={"row"}>
            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="First name"
                  size="large"
                  type={'text'}
                  value={fname}
                  onChange={nameFChange}
                />
              </FormControl></Grid>

            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Last name"
                  size="large"
                  type={'text'}
                  value={lname}
                  onChange={nameLChange}
                />
              </FormControl></Grid></Grid>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Grid container direction={"row"}>
            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Email"
                  size="large"
                  type={'text'}
                  value={email}
                  onChange={emailChange}
                />
              </FormControl>
            </Grid>
            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Password"
                  size="large"
                  type={'password'}
                  value={password}
                  onChange={passwordChange}
                />
              </FormControl>
            </Grid></Grid>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Grid container direction={"row"}>
            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Username"
                  size="large"
                  type={'text'}
                  value={uname}
                  onChange={handleUnameChange}
                />
              </FormControl>
            </Grid>
            <Grid md={6}>
              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Phone"
                  size="large"
                  type={'text'}
                  value={phone}
                  onChange={phoneChange}
                />
              </FormControl>
            </Grid>
          </Grid>
          <br></br>
          <Grid container direction={"row"}>
            <Grid md={6}>

              <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                <TextField
                  id="standard-adornment-fname"
                  label="Mobile"
                  size="large"
                  type={'text'}
                  value={mobile}
                  onChange={mobileChange}
                />
              </FormControl>
            </Grid>
            <Grid md={6}>
              <br></br>
              <Checkbox
                checked={isAdmin}
                onChange={handleAdminChange}
                sx={{ '& .MuiSvgIcon-root': { fontSize: 28 } }}
              /> Is Administrator
            </Grid></Grid>

          <button className="block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold" onClick={clickRegister} >&nbsp;SAVE</button>



        </div>


      </div>
      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}