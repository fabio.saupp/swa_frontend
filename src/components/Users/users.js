import {
  Checkbox, FormControl, Grid, Snackbar,
  Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Tooltip
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import add from '../../assets/add.png';
import deletes from '../../assets/delete.png';
import edit from '../../assets/edit.png';
import { deleteUserById, editUserTobk, getCurrentUserData, getCustomerData, getUserById, getUserDataByCustomer } from '../../services/services';
import AddUsers from './addUsers';
import EditUsers from './editUsers';
export default function Users() {

  const [logButtonName, setlogButtonName] = React.useState((localStorage.getItem("email") === "" && localStorage.getItem("email") === undefined
    && localStorage.getItem("email") === null) ? "LOGIN" : "LOGOUT");
  const [isOpen, setIsOpen] = React.useState(false);
  const [value, setValue] = React.useState(1);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackMessage, setSnackMessage] = React.useState('');
  const [userData, setUserData] = React.useState([]);
  const [currentUserData, setCurrentUserData] = React.useState({});
  const [isAddUserOpen, setIsAddUserOpen] = React.useState(false);
  const [isEditUserOpen, setIsEditUserOpen] = React.useState(false);
  const [isViewContractorOpen, setIsViewContractorOpen] = React.useState(false);

  const [eUserId, seteUserId] = React.useState('');
  const [eCustomerId, seteCustomerId] = React.useState('');
  const [efname, seteFname] = React.useState("");
  const [elname, seteLname] = React.useState("");
  const [eemail, seteEmail] = React.useState('');
  const [euname, seteUname] = React.useState('');


  const [ephone, setePhone] = React.useState('');
  const [emobile, seteMobile] = React.useState('');
  const [epassword, setePassword] = React.useState('');

  const [eIsAdmin, seteIsAdmin] = React.useState('');


  const [selectValue, setSelectValue] = React.useState('');
  const [customerData, setCustomerData] = React.useState([]);
  const [emailError, setEmailError] = React.useState('');
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [cpassword, setCPassword] = React.useState("");
  const [fname, setFName] = React.useState("");
  const [lname, setLName] = React.useState("");
  const [uname, setUName] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [mobile, setMobile] = React.useState("");
  const [isAdmin, setIsAdmin] = React.useState(false);

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const handleChangeCustomer = (event) => {
    setSelectValue(event.target.value);
  };

  const handleAdminChange = (event) => {
    setIsAdmin(event.target.checked);
  };

  const handleUnameChange = (event) => {
    setUName(event.target.value);
  };


  const passwordChange = (event) => {
    setPassword(event.target.value);
  }

  const phoneChange = (event) => {
    setPhone(event.target.value);
  }

  const mobileChange = (event) => {
    setMobile(event.target.value);
  }

  const cpasswordChange = (event) => {
    setCPassword(event.target.value);
  }

  const emailChange = (event) => {
    setEmail(event.target.value);
    if (!ValidateEmail(event.target.value)) {
      setEmailError('Enter valid Email!');
    } else {
      setEmailError('');
    }
  }

  const nameFChange = (event) => {
    setFName(event.target.value);
  }

  const nameLChange = (event) => {
    setLName(event.target.value);
  }

  const clickRegister = () => {

    if (email === "" || email === undefined || password === "" || password === undefined ||
      fname === "" || fname === undefined || lname === "" || lname === undefined ||
      phone === "" || phone === undefined || mobile === "" || mobile === undefined ||
      selectValue === "" || selectValue === undefined) {
      setSnackMessage('Please fill out this field');
      setOpenSnack(true);
    } else if (!ValidateEmail(email)) {
      setSnackMessage('Email is not valid!!');
      setOpenSnack(true);
      return false;
    } else if (!phonenumber(mobile)) {
      setSnackMessage('Mobile is not valid!!');
      setOpenSnack(true);
      return false;
    } else if (!phonenumber(phone)) {
      setSnackMessage('Phone is not valid!!');
      setOpenSnack(true);
      return false;
    } else {
      editUserTobk(localStorage.getItem("userId"), selectValue, fname, lname, email, password, uname, phone, mobile, isAdmin).then(res => {
        console.log(res)
        if (res.ok) {
          setFName("");
          setLName("");
          setEmail("");
          setPassword("");
          setSnackMessage('User updated successfully!');
          setOpenSnack(true);
        } else {
          res.text().then(text => {
            let err = JSON.parse(text);
            console.log(err);

            if (err.error) {
              setSnackMessage(err.errors[0].defaultMessage);
            } else {
              setSnackMessage(err.message);
            }
            setOpenSnack(true);
          })
        }

      })
        .catch(error => {
          console.log("Regiter failed" + error);
          setOpenSnack(true);
          setSnackMessage('User updation Failed!');
        })
    }
  }

  function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return (true)
    }
    return (false)
  }

  function phonenumber(mobile) {
    var phoneno = /^\d{10}$/;
    if (mobile.match(phoneno)) {
      return true;
    }
    else {
      return false;
    }
  }


  function toggleModal() {
    if (logButtonName === 'LOGOUT') {
      setIsOpen(!isOpen);
    } else {
      setIsOpen(!isOpen);
    }
  }

  React.useEffect(() => {
    getUserDataByCustomer("").then(resp => {
      console.log(resp.data);
      setUserData(resp.data);
    }).catch(error => {
      console.log("login user err ", error);
    });

    getCurrentUserData().then(resp => {
      console.log(resp.data);
      setCurrentUserData(resp.data);
      setSelectValue(resp.data.customerId);
      setFName(resp.data.firstName);
      setLName(resp.data.lastName);
      setEmail(resp.data.email);
      setUName(resp.data.username);
      setPassword(resp.data.password);
      setPhone(resp.data.phone);
      setMobile(resp.data.mobile);
      setIsAdmin(resp.data.role === "ADMIN" ? true : false);

    }).catch(error => {
      console.log("login user err ", error);
    });

    getCustomerData("").then(resp => {
      console.log(resp.data);
      setCustomerData(resp.data);
    }).catch(error => {
      console.log("login user err ", error);
    });

  }, []);

  const loginHandler = (value) => {
    setIsLoggedIn(value);
  }
  React.useEffect(() => {
    getLoggedInStatus();

  }, [value]);


  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };

  function getLoggedInStatus() {
    if (localStorage.getItem("email") !== "" && localStorage.getItem("email") !== undefined
      && localStorage.getItem("email") !== null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }
  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1000px !important',
      height: '800px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogForViewMovie = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1200px !important',
      height: '900px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  const DialogAddSlot = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '800px !important',
      height: '600px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

  const DialogTitleForModal = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  DialogTitleForModal.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  function toggleAddUserModal() {
    setIsAddUserOpen(!isAddUserOpen);
    if (isAddUserOpen === true) {
      getUserDataByCustomer("").then(resp => {
        console.log(resp.data);
        setUserData(resp.data);

      }).catch(error => {
        console.log("login user err " + error);
      });
    }
  }


  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const columns = [
    { id: 'username', label: 'USERNAME', maxWidth: 100 },
    { id: 'email', label: 'EMAIL', maxWidth: 100 },
    { id: 'action', label: 'ACTION', maxWidth: 100 }
  ];


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const editUser = (id, pos) => {
    getUserById(id).then(resp => {
      console.log(JSON.stringify(resp));
      console.log(resp.data);
      seteUserId(id);
      seteCustomerId(resp.data.customerId);
      seteFname(resp.data.firstName);
      seteLname(resp.data.lastName);
      seteEmail(resp.data.email);
      seteUname(resp.data.username);
      setePassword(resp.data.password);
      setePhone(resp.data.phone);
      seteMobile(resp.data.mobile);
      seteIsAdmin(resp.data.role);
      toggleEditUserModal();
    }).catch(error => {
      console.log("login user err ", error);
    });


  }

  function toggleEditUserModal() {
    setIsEditUserOpen(!isEditUserOpen);
    if (isEditUserOpen === true) {
      getUserDataByCustomer("").then(resp => {
        console.log(resp.data);
        setUserData(resp.data);

      }).catch(error => {
        console.log("login user err " + error);
        setOpenSnack(true);
        setSnackMessage(error.response.data.message);
      });
    }
  }



  const deleteUser = (id) => {
    console.log(id);
    deleteUserById(id).then(resp => {
      if (resp.status === 500) {
        setSnackMessage('Error occured during delete user');
        setOpenSnack(true);
      } else if (resp.status === 403) {
        setSnackMessage('You dont have permission to delete user');
        setOpenSnack(true);
      } else {
        console.log(resp);
        setSnackMessage('User deleted successfully');
        setOpenSnack(true);


        getUserDataByCustomer("").then(resp => {
          console.log(resp.data);
          setUserData(resp.data);

        }).catch(error => {
          console.log("login user err " + error);
          setOpenSnack(true);
          setSnackMessage(error.response.data.message);
        });

      }

    }).catch(error => {
      if (error.response.status === 403) {
        setSnackMessage('You dont have permission to delete user');
        setOpenSnack(true);
      } else {
        setOpenSnack(true);
        setSnackMessage("Deletion failed: " + error.response.data.message);
        console.log("login user err " + error);
      }

    })
  }

  return (
    <React.Fragment>

      {((localStorage.getItem("role") !== "" && localStorage.getItem("role") !== undefined
        && localStorage.getItem("role") === "ADMIN")) ? (




        <Grid container direction="row" spacing={1}
          style={{ paddingLeft: '100px' }} >
          <Grid md={1}></Grid>
          <Grid md={10}>
            <br></br><br></br><br></br>


            <Image src={add} onClick={toggleAddUserModal} style={{ float: 'right', height: '80px', marginRight: '3%', marginTop: '-10px', cursor: 'pointer' }}></Image>
            <br></br><br></br>

            {
              Object.keys(userData).map((each, value) =>
                <>
                  <Typography variant='button' sx={{ fontSize: '20px' }} >{each}</Typography>
                  <TableContainer >
                    <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto " >
                      <TableHead>
                        <TableRow class="bg-gray-900 text-white uppercase leading-normal">
                          {columns.map((column) => (
                            <TableCell
                              key={column.id}
                              align="center"
                              style={{ maxWidth: column.maxWidth }}
                              class="py-3 px-6 text-center bg-gray-900"
                            >
                              {column.label}
                            </TableCell>
                          ))}
                        </TableRow>
                      </TableHead>
                      <TableBody class="text-gray-200 bg-gray-700 text-sm font-light  ">
                        {userData[each] && userData[each].length > 0 ? (userData[each]
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map((row, ind) => {
                            return (
                              <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                                {columns.map((column) => {
                                  const id = row["id"];
                                  const value = row[column.id];
                                  return (
                                    <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                      {/* {column.format && typeof value === 'number'
                        ? column.format(value)
                        : value} */}


                                      {(column.id === 'action') ? (
                                        <>
                                          <Tooltip title="Edit user">
                                            <IconButton aria-label="edit" onClick={(e) => editUser(id, ind)} style={{ color: '#566573' }} size="small">

                                              <Image src={edit} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton>
                                          </Tooltip>&nbsp;
                                          <Tooltip title="Delete user">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => deleteUser(id)} aria-label="delete" size="small">

                                              <Image src={deletes} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>&nbsp;
                                        </>
                                      ) :
                                        (column.id === 'sNo') ? (
                                          ind + 1
                                        ) : value}
                                    </TableCell>
                                  );
                                })}
                              </TableRow>
                            );
                          })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  {/* <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={each.value.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        class="bg-gray-900 text-white uppercase leading-normal"
                      /> */}

                  <br></br><br></br>
                </>
              )


            }
            <br></br><br></br><br></br><br></br>
          </Grid>
          <Grid md={1}></Grid>






        </Grid>
      ) : (
        <div className="w-full py-10 px-15 md:px-10">
          <div className="text-center mb-10">
            <h1 className="font-bold text-gray-900 p-50">MY PROFILE</h1>
            <br></br><br></br>

            <Grid container direction={"row"}>
              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="First name"
                    size="large"
                    type={'text'}
                    value={fname}
                    onChange={nameFChange}
                  />
                </FormControl></Grid>

              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Last name"
                    size="large"
                    type={'text'}
                    value={lname}
                    onChange={nameLChange}
                  />
                </FormControl></Grid></Grid>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <Grid container direction={"row"}>
              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Email"
                    size="large"
                    type={'text'}
                    value={email}
                    onChange={emailChange}
                  />
                </FormControl>
              </Grid>
              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Password"
                    size="large"
                    type={'password'}
                    value={password}
                    onChange={passwordChange}
                  />
                </FormControl>
              </Grid></Grid>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <Grid container direction={"row"}>
              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Username"
                    size="large"
                    type={'text'}
                    value={uname}
                    onChange={handleUnameChange}
                  />
                </FormControl>
              </Grid>
              <Grid md={6}>
                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Phone"
                    size="large"
                    type={'text'}
                    value={phone}
                    onChange={phoneChange}
                  />
                </FormControl>
              </Grid>
            </Grid>
            <br></br>
            <Grid container direction={"row"}>
              <Grid md={6}>

                <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '90%' }}>
                  <TextField
                    id="standard-adornment-fname"
                    label="Mobile"
                    size="large"
                    type={'text'}
                    value={mobile}
                    onChange={mobileChange}
                  />
                </FormControl>
              </Grid>
              <Grid md={6}>
                <br></br>
                <Checkbox
                  checked={isAdmin}
                  onChange={handleAdminChange}
                  sx={{ '& .MuiSvgIcon-root': { fontSize: 28 } }}
                /> Is Administrator
              </Grid></Grid>

            <button className="block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold" onClick={clickRegister} >&nbsp;EDIT USER</button>



          </div>


        </div>
      )}


      <DialogAddSlot
        onClose={toggleAddUserModal}
        aria-labelledby="customized-dialog-title"
        open={isAddUserOpen}
      >

        <AddUsers toggleModal={toggleAddUserModal} />

      </DialogAddSlot>

      <DialogAddSlot
        onClose={toggleEditUserModal}
        aria-labelledby="customized-dialog-title"
        open={isEditUserOpen}
      >

        <EditUsers editUserModal={toggleEditUserModal} eUserId={eUserId} eCustomerId={eCustomerId}
          efname={efname} elname={elname} eemail={eemail} euname={euname} epassword={epassword}
          ephone={ephone} emobile={emobile} eIsAdmin={eIsAdmin} />

      </DialogAddSlot>


      {/* <DialogAddSlot
        onClose={toggleViewContractorModal}
        aria-labelledby="customized-dialog-title"
        open={isViewContractorOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          VIEW DETAILS
        </DialogTitleForModal>





        <ViewContractor editContractorModal={toggleViewContractorModal} eCustomerId={eCustomerId} 
        euserData={euserData} ecustomerData={ecustomerData} esdate={esdate} eedate={eedate} efeatureA={efeatureA}
        efeatureB={efeatureB} efeatureC={efeatureC} eipNumber1={eipNumber1} 
        eipNumber2={eipNumber2} eipNumber3={eipNumber3} eversion={eversion} elicenseKey={elicenseKey}/>

      </DialogAddSlot> */}

      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}