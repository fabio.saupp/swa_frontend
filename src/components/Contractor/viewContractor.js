import { FormControl, Grid, InputLabel, ListItemText, MenuItem, OutlinedInput, Select, Snackbar, TextField } from '@mui/material';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import { DesktopDatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import customer from '../../assets/customer.svg';
import { getAllUsersByCustomerId, getCustomerData } from '../../services/services';


export default function ViewContractor({ editContractorModal, econtractorId, euserData, ecustomerData, esdate
  , eedate, efeatureA, efeatureB, efeatureC, eipNumber1, eipNumber2, eipNumber3, eversion, elicenseKey }) {
  console.log(econtractorId, euserData, esdate
    , eedate, efeatureA, efeatureB, efeatureC, eipNumber1, eipNumber2, eipNumber3, eversion, elicenseKey);


  const [selectValue, setSelectValue] = React.useState(ecustomerData);

  const [customerData, setCustomerData] = React.useState([]);

  const [selectUserValue, setSelectUserValue] = React.useState(euserData);
  const [userData, setUserData] = React.useState([]);

  const [sdate, setSDate] = React.useState(esdate);
  const [edate, setEDate] = React.useState(eedate);


  const [ipNumber1, setIpNumber1] = React.useState(eipNumber1);
  const [ipNumber2, setIpNumber2] = React.useState(eipNumber2);
  const [ipNumber3, setIpNumber3] = React.useState(eipNumber3);

  const [featureA, setFeatureA] = React.useState(efeatureA);
  const [featureB, setFeatureB] = React.useState(efeatureB);
  const [featureC, setFeatureC] = React.useState(efeatureC);

  const [version, setVersion] = React.useState(eversion);
  const [licenseKey, setLicenseKey] = React.useState(elicenseKey);

  const [snackMessage, setSnackMessage] = React.useState('');
  const [openSnack, setOpenSnack] = React.useState(false);
  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };


  const handleVersionChange = (e) => {
    setVersion(e.target.value);
  };

  const handleLicenseKeyChange = (e) => {
    setLicenseKey(e.target.value);
  };

  const handleIpNumber2Change = (e) => {
    setIpNumber2(e.target.value);
  };
  const handleIpNumber1Change = (e) => {
    setIpNumber1(e.target.value);
  };
  const handleIpNumber3Change = (e) => {
    setIpNumber3(e.target.value);
  };

  const handleFeatureAChange = (e) => {
    setFeatureA(e.target.value);
  };

  const handleFeatureBChange = (e) => {
    setFeatureB(e.target.value);
  };

  const handleFeatureCChange = (e) => {
    setFeatureC(e.target.value);
  };


  const handleChange = (event) => {
    setSelectValue(event.target.value);
  };

  const handleEDateChange = (e) => {
    setEDate(e)
  }

  const handleSDateChange = (e) => {
    setSDate(e)
  }

  const handleResponsibleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectUserValue(typeof value === 'string' ? value.split(',') : value);
  };

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  React.useEffect(() => {
    getCustomerData("").then(resp => {
      console.log(resp.data);
      setCustomerData(resp.data);
    }).catch(error => {
      console.log("login user err ", error);
    });

    getAllUsersByCustomerId(selectValue).then(resp => {
      console.log(JSON.stringify(resp));
      console.log(resp.data);
      setUserData(resp.data);
    }).catch(error => {
      console.log("login user err ", error);
    });

  }, []);



  function editIdCustomer() {

    // console.log(econtractorId, customerName, department, address);
    // if (customerName === "" || customerName === undefined || department === "" || department === undefined ||
    //   address === "" || address === undefined) {
    //   setSnackMessage('Fields cannot be blank');
    //   setOpenSnack(true);
    // } else {

    //   editCustomer(econtractorId, customerName, department, address).then(resp => {
    //     console.log(resp);
    //     let data = resp.data;
    //     console.log(data);
    //     setSnackMessage('Customer updated successfully');
    //     setOpenSnack(true);
    //     editContractorModal();
    //   });
    // }
  }
  return (
    <React.Fragment>
      <DialogContent>
        <Grid container >
          <Grid item xs={5}>
            <Image src={customer} style={{ marginLeft: '20px' }} />

          </Grid>
          <Grid item xs={1}></Grid>
          <Grid item xs={6}>
            <br></br>
            <FormControl fullWidth >
              <InputLabel id="demo-multiple-checkbox-label"  >Select customer</InputLabel>
              <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                size="large"
                value={selectValue}
                //onChange={handleChange}
                input={<OutlinedInput label="Select customer" />}
              // input={<OutlinedInput label="Select services" />}
              // renderValue={(selected) => setSelectValue(selected)}
              // MenuProps={MenuProps}
              >
                {customerData.map((eachItem) => (
                  <MenuItem key={eachItem.name} value={eachItem.id}>
                    {eachItem.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <br></br><br></br>
            <LocalizationProvider fullWidth dateAdapter={AdapterDateFns} style={{ maxWidth: '30% !important' }}>
              <DesktopDatePicker
                label="Start date"
                fullWidth
                value={sdate}
                disablePast="true"
                //onChange={handleSDateChange}
                renderInput={(params) => <TextField {...params} style={{ width: '48%' }} />}
              /></LocalizationProvider> &nbsp;&nbsp;&nbsp;

            <LocalizationProvider fullWidth dateAdapter={AdapterDateFns} style={{ maxWidth: '30% !important' }}>
              <DesktopDatePicker
                label="End date"
                fullWidth
                value={edate}
                disablePast="true"
                //onChange={handleEDateChange}
                renderInput={(params) => <TextField {...params} style={{ width: '48%' }} />}
              /></LocalizationProvider> &nbsp;&nbsp;&nbsp;

            <br></br><br></br>
            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '49%' }}>
              <TextField
                id="standard-adornment-fname"
                label="VERSION"
                size="large"
                type={'text'}
                value={version}
              //onChange={handleVersionChange}
              />
            </FormControl>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <FormControl style={{ width: '48%' }} >
              <InputLabel id="demo-multiple-checkbox-label"  >Responsible for:</InputLabel>
              <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                size="large"
                value={selectUserValue}
                //onChange={handleResponsibleChange}
                input={<OutlinedInput label="Responsible for:" />}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
              >
                {userData.map((eachItem) => (
                  <MenuItem key={eachItem.username} value={eachItem.username}>
                    <Checkbox checked={selectUserValue.indexOf(eachItem.username) > -1} />
                    <ListItemText primary={eachItem.username} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <br></br><br></br>




            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                label="IP NUMBER"
                size="large"
                type={'text'}
                value={ipNumber1}
              //onChange={handleIpNumber1Change}
              />
            </FormControl>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                label="FEATURE A"
                size="large"
                type={'text'}
                value={featureA}
              //onChange={handleFeatureAChange}
              />
            </FormControl>
            <br></br><br></br>
            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                label="IP NUMBER"
                size="large"
                type={'text'}
                value={ipNumber2}
              //onChange={handleIpNumber2Change}
              />
            </FormControl>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                label="FEATURE B"
                size="large"
                type={'text'}
                value={featureB}
              //onChange={handleFeatureBChange}
              />
            </FormControl>
            <br></br><br></br>

            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                size="large"
                label="IP NUMBER"
                type={'text'}
                value={ipNumber3}
              //onChange={handleIpNumber3Change}
              />
            </FormControl>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <FormControl required={true} variant="standard" style={{ textAlign: 'center', width: '48%' }}>
              <TextField
                id="standard-adornment-fname"
                label="FEATURE C"
                size="large"
                type={'text'}
                value={featureC}
              //onChange={handleFeatureCChange}
              />
            </FormControl>
            <br></br><br></br>
            <FormControl required={true} fullWidth variant="standard" style={{ textAlign: 'center' }}>
              <TextField
                id="outlined-multiline-static"
                label="License key"
                multiline
                rows={4}
                value={licenseKey}
              //onChange={handleLicenseKeyChange}
              /></FormControl>
          </Grid>





        </Grid>
      </DialogContent>
      <DialogActions align='center'>
        <Button variant="contained" style={{ backgroundColor: "#566573" }} onClick={editContractorModal}>&nbsp;CLOSE</Button>
      </DialogActions>
      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}