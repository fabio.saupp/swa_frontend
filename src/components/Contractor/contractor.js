import { Grid, Snackbar, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import add from '../../assets/add.png';
import deletes from '../../assets/delete.png';
import details from '../../assets/details.png';
import edit from '../../assets/edit.png';
import { deleteContractors, getContractorById, getContractorData, getContractorsByUserId } from '../../services/services';
import AddContractor from './addContractor';
import EditContractor from './editContractor';
import ViewContractor from './viewContractor';
export default function Contractor() {

  const [logButtonName, setlogButtonName] = React.useState((localStorage.getItem("email") === "" && localStorage.getItem("email") === undefined
    && localStorage.getItem("email") === null) ? "LOGIN" : "LOGOUT");
  const [isOpen, setIsOpen] = React.useState(false);
  const [value, setValue] = React.useState(1);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackMessage, setSnackMessage] = React.useState('');
  const [contractorData, setContractorData] = React.useState([]);
  const [contractsData, setContractsData] = React.useState([]);
  const [isAddContractorOpen, setIsAddContractorOpen] = React.useState(false);
  const [isEditContractorOpen, setIsEditContractorOpen] = React.useState(false);
  const [isViewContractorOpen, setIsViewContractorOpen] = React.useState(false);
  const [eContractorId, seteContractorId] = React.useState('');
  const [euserData, seteUserData] = React.useState([]);
  const [ecustomerData, seteCustomerData] = React.useState({});
  const [esdate, seteSDate] = React.useState('');
  const [eedate, seteEDate] = React.useState('');


  const [eipNumber1, seteIpNumber1] = React.useState('');
  const [eipNumber2, seteIpNumber2] = React.useState('');
  const [eipNumber3, seteIpNumber3] = React.useState('');

  const [efeatureA, seteFeatureA] = React.useState('');
  const [efeatureB, seteFeatureB] = React.useState('');
  const [efeatureC, seteFeatureC] = React.useState('');

  const [eversion, seteVersion] = React.useState('');
  const [elicenseKey, seteLicenseKey] = React.useState('');


  function toggleModal() {
    if (logButtonName === 'LOGOUT') {
      setIsOpen(!isOpen);
    } else {
      setIsOpen(!isOpen);
    }
  }

  React.useEffect(() => {

    if (localStorage.getItem("role") === "ADMIN") {
      getContractorData("").then(resp => {
        console.log(resp.data);
        setContractorData(resp.data);
      }).catch(error => {
        console.log("login user err ", error);
      });
    } else {
      getContractorsByUserId().then(resp => {
        console.log(resp.data);
        setContractsData(resp.data);
      }).catch(error => {
        console.log("login user err ", error);
      });
    }


  }, []);

  const loginHandler = (value) => {
    setIsLoggedIn(value);
  }
  React.useEffect(() => {
    getLoggedInStatus();

  }, [value]);


  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };

  function getLoggedInStatus() {
    if (localStorage.getItem("email") !== "" && localStorage.getItem("email") !== undefined
      && localStorage.getItem("email") !== null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }
  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1000px !important',
      height: '800px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogForViewMovie = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1200px !important',
      height: '900px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  const DialogAddSlot = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1500px !important',
      height: '800px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

  const DialogTitleForModal = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  DialogTitleForModal.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  function toggleAddContractorModal() {
    setIsAddContractorOpen(!isAddContractorOpen);
    if (isAddContractorOpen === true) {
      getContractorData("").then(resp => {
        console.log(resp.data);
        setContractorData(resp.data);

      }).catch(error => {
        console.log("login user err " + error);
      });
    }
  }


  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const columns = [
    { id: 'startDate', label: 'CONTRACT START', maxWidth: 100 },
    { id: 'endDate', label: 'CONTRACT END', maxWidth: 30 },
    { id: 'version', label: 'VERSION', maxWidth: 100 },
    { id: 'action', label: 'ACTION', maxWidth: 100 }
  ];


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const editContractor = (id, pos) => {
    seteContractorId(id);
    getContractorById(id).then(resp => {
      console.log(JSON.stringify(resp));
      console.log(resp.data);
      let userNames = [];
      resp.data.users.map(each => userNames.push(each.username));

      seteUserData(userNames);
      seteCustomerData(resp.data.customer.id);
      seteSDate(resp.data.startDate);
      seteEDate(resp.data.endDate);
      seteFeatureA(resp.data.feature1);
      seteFeatureB(resp.data.feature2);
      seteFeatureC(resp.data.feature3);
      seteIpNumber1(resp.data.ip1);
      seteIpNumber2(resp.data.ip2);
      seteIpNumber3(resp.data.ip3);
      seteVersion(resp.data.version);
      seteLicenseKey(resp.data.licenseKey);
      toggleEditContractorModal();
    }).catch(error => {
      console.log("login user err ", error);
    });


  }

  function toggleEditContractorModal() {
    setIsEditContractorOpen(!isEditContractorOpen);
    if (isEditContractorOpen === true) {


      if (localStorage.getItem("role") !== "" && localStorage.getItem("role") !== undefined
        && localStorage.getItem("role") === "ADMIN") {
        getContractorData("").then(resp => {
          console.log(resp.data);
          setContractorData(resp.data);

        }).catch(error => {
          console.log("login user err " + error);
          setOpenSnack(true);
          setSnackMessage(error.response.data.message);
        });
      } else {
        getContractorsByUserId().then(resp => {
          console.log(resp.data);
          setContractsData(resp.data);
        }).catch(error => {
          console.log("login user err ", error);
        });
      }
    }
  }



  const viewContractor = (id, pos) => {
    seteContractorId(id);
    getContractorById(id).then(resp => {
      console.log(JSON.stringify(resp));
      console.log(resp.data);
      let userNames = [];
      resp.data.users.map(each => userNames.push(each.username));

      seteUserData(userNames);
      seteCustomerData(resp.data.customer.id);
      seteSDate(resp.data.startDate);
      seteEDate(resp.data.endDate);
      seteFeatureA(resp.data.feature1);
      seteFeatureB(resp.data.feature2);
      seteFeatureC(resp.data.feature3);
      seteIpNumber1(resp.data.ip1);
      seteIpNumber2(resp.data.ip2);
      seteIpNumber3(resp.data.ip3);
      seteVersion(resp.data.version);
      seteLicenseKey(resp.data.licenseKey);
      toggleViewContractorModal();
    }).catch(error => {
      console.log("login user err ", error);
    });


  }

  function toggleViewContractorModal() {
    setIsViewContractorOpen(!isViewContractorOpen);
    // if (isViewContractorOpen === true) {
    //   getContractorData("").then(resp => {
    //     console.log(resp.data);
    //     setContractorData(resp.data);

    //   }).catch(error => {
    //     console.log("login user err " + error);
    //     setOpenSnack(true);
    //     setSnackMessage(error.response.data.message);
    //   });
    // }
  }

  const deleteContractor = (id) => {
    console.log(id);
    deleteContractors(id).then(resp => {
      if (resp.status === 500) {
        setSnackMessage('Error occured during delete contractor');
        setOpenSnack(true);
      } else {
        console.log(resp);
        setSnackMessage('Contractor deleted successfully');
        setOpenSnack(true);


        getContractorData("").then(resp => {
          console.log(resp.data);
          setContractorData(resp.data);

        }).catch(error => {
          console.log("login user err " + error);
          setOpenSnack(true);
          setSnackMessage(error.response.data.message);
        });

      }

    }).catch(error => {
      setOpenSnack(true);
      setSnackMessage("Deletion failed: " + error.response.data.message);
      console.log("login user err " + error);
    })
  }

  return (
    <React.Fragment>

      {((localStorage.getItem("role") !== "" && localStorage.getItem("role") !== undefined
        && localStorage.getItem("role") === "ADMIN")) ? (




        <Grid container direction="row" spacing={1}
          style={{ paddingLeft: '100px' }} >
          <Grid md={1}></Grid>
          <Grid md={10}>
            <br></br><br></br><br></br>


            <Image src={add} onClick={toggleAddContractorModal} style={{ float: 'right', height: '80px', marginRight: '3%', marginTop: '-10px', cursor: 'pointer' }}></Image>
            <br></br><br></br>

            {
              contractorData.length > 0 ? contractorData.map(each =>
                <>
                  <Typography variant='button' sx={{ fontSize: '20px' }} >{each.key.name}</Typography>
                  <TableContainer >
                    <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto " >
                      <TableHead>
                        <TableRow class="bg-gray-900 text-white uppercase leading-normal">
                          {columns.map((column) => (
                            <TableCell
                              key={column.id}
                              align="center"
                              style={{ maxWidth: column.maxWidth }}
                              class="py-3 px-6 text-center bg-gray-900"
                            >
                              {column.label}
                            </TableCell>
                          ))}
                        </TableRow>
                      </TableHead>
                      <TableBody class="text-gray-200 bg-gray-700 text-sm font-light  ">
                        {each && each.value.length > 0 ? (each.value
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map((row, ind) => {
                            return (
                              <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                                {columns.map((column) => {
                                  const id = row["id"];
                                  const value = row[column.id];
                                  return (
                                    <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                      {/* {column.format && typeof value === 'number'
                        ? column.format(value)
                        : value} */}


                                      {(column.id === 'action') ? (
                                        <>
                                          <Tooltip title="Edit contractors">
                                            <IconButton aria-label="edit" onClick={(e) => editContractor(id, ind)} style={{ color: '#566573' }} size="small">

                                              <Image src={edit} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton>
                                          </Tooltip>&nbsp;
                                          <Tooltip title="Delete contractors">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => deleteContractor(id)} aria-label="delete" size="small">

                                              <Image src={deletes} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>&nbsp;
                                          <Tooltip title="View details">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => viewContractor(id, ind)} aria-label="delete" size="small">

                                              <Image src={details} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>
                                        </>
                                      ) :
                                        (column.id === 'sNo') ? (
                                          ind + 1
                                        ) : value}
                                    </TableCell>
                                  );
                                })}
                              </TableRow>
                            );
                          })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  {/* <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={each.value.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        class="bg-gray-900 text-white uppercase leading-normal"
                      /> */}

                  <br></br><br></br>
                </>
              ) : ""


            }
            <br></br><br></br><br></br><br></br>
          </Grid>
          <Grid md={1}></Grid>






        </Grid>
      ) : (
        <Grid container direction="row" spacing={1}
          style={{ paddingLeft: '100px' }} >
          <Grid md={1}></Grid>
          <Grid md={10}>
            <br></br><br></br><br></br>


            <>
              <TableContainer >
                <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto " >
                  <TableHead>
                    <TableRow class="bg-gray-900 text-white uppercase leading-normal">
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align="center"
                          style={{ maxWidth: column.maxWidth }}
                          class="py-3 px-6 text-center bg-gray-900"
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody class="text-gray-200 bg-gray-700 text-sm font-light  ">
                    {contractsData && contractsData.length > 0 ? (contractsData
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row, ind) => {
                        return (
                          <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                            {columns.map((column) => {
                              const id = row["id"];
                              const value = row[column.id];
                              return (
                                <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                  {/* {column.format && typeof value === 'number'
                        ? column.format(value)
                        : value} */}


                                  {(column.id === 'action') ? (
                                    <>
                                      <Tooltip title="Edit contractors">
                                        <IconButton aria-label="edit" onClick={(e) => editContractor(id, ind)} style={{ color: '#566573' }} size="small">

                                          <Image src={edit} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                        </IconButton>
                                      </Tooltip>&nbsp;

                                      <Tooltip title="View details">
                                        <IconButton style={{ color: 'red' }} onClick={(e) => viewContractor(id, ind)} aria-label="delete" size="small">

                                          <Image src={details} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                        </IconButton></Tooltip>
                                    </>
                                  ) :
                                    (column.id === 'sNo') ? (
                                      ind + 1
                                    ) : value}
                                </TableCell>
                              );
                            })}
                          </TableRow>
                        );
                      })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
              {/* <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={each.value.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        class="bg-gray-900 text-white uppercase leading-normal"
                      /> */}

              <br></br><br></br>
            </>

            <br></br><br></br><br></br><br></br>
          </Grid>
          <Grid md={1}></Grid>






        </Grid>
      )}


      <DialogAddSlot
        onClose={toggleAddContractorModal}
        aria-labelledby="customized-dialog-title"
        open={isAddContractorOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          ADD NEW CONTRACT
        </DialogTitleForModal>

        <AddContractor addContractorModal={toggleAddContractorModal} />

      </DialogAddSlot>

      <DialogAddSlot
        onClose={toggleEditContractorModal}
        aria-labelledby="customized-dialog-title"
        open={isEditContractorOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          EDIT CONTRACT
        </DialogTitleForModal>





        <EditContractor editContractorModal={toggleEditContractorModal} econtractorId={eContractorId}
          euserData={euserData} ecustomerData={ecustomerData} esdate={esdate} eedate={eedate} efeatureA={efeatureA}
          efeatureB={efeatureB} efeatureC={efeatureC} eipNumber1={eipNumber1}
          eipNumber2={eipNumber2} eipNumber3={eipNumber3} eversion={eversion} elicenseKey={elicenseKey} />

      </DialogAddSlot>


      <DialogAddSlot
        onClose={toggleViewContractorModal}
        aria-labelledby="customized-dialog-title"
        open={isViewContractorOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          VIEW DETAILS
        </DialogTitleForModal>





        <ViewContractor editContractorModal={toggleViewContractorModal} econtractorId={eContractorId}
          euserData={euserData} ecustomerData={ecustomerData} esdate={esdate} eedate={eedate} efeatureA={efeatureA}
          efeatureB={efeatureB} efeatureC={efeatureC} eipNumber1={eipNumber1}
          eipNumber2={eipNumber2} eipNumber3={eipNumber3} eversion={eversion} elicenseKey={elicenseKey} />

      </DialogAddSlot>

      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}