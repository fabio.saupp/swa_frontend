import React, { useState } from "react";

//All the svg files
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import logo from "../../assets/logos.png";
import PowerOff from "../../assets/power-off-solid.svg";
import { logoutUser } from "../../services/services";

const Container = styled.div`
  position: fixed;

  .active {
    border-right: 4px solid var(--white);

    img {
      filter: invert(100%) sepia(0%) saturate(0%) hue-rotate(93deg)
        brightness(103%) contrast(103%);
    }
  }
`;

const Button = styled.button`
  background-color: var(--black);
  border: none;
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 50%;
  margin: 0.5rem 0 0 0.5rem;
  cursor: pointer;

  display: flex;
  justify-content: center;
  align-items: center;

  position: relative;

  &::before,
  &::after {
    content: "";
    background-color: var(--white);
    height: 2px;
    width: 1rem;
    position: absolute;
    transition: all 0.3s ease;
  }

  &::before {
    top: ${(props) => (props.clicked ? "1.5" : "1rem")};
    transform: ${(props) => (props.clicked ? "rotate(135deg)" : "rotate(0)")};
  }

  &::after {
    top: ${(props) => (props.clicked ? "1.2" : "1.5rem")};
    transform: ${(props) => (props.clicked ? "rotate(-135deg)" : "rotate(0)")};
  }
`;

const SidebarContainer = styled.div`
  background-color: var(--black);
  width: 3.5rem;
  height: 80vh;
  margin-top: 1rem;
  border-radius: 0 30px 30px 0;
  padding: 1rem 0;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  position: relative;
`;

const Logo = styled.div`
  width: 2rem;

  img {
    width: 100%;
    height: auto;
  }
`;

const SlickBar = styled.ul`
  color: var(--white);
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--black);

  padding: 2rem 0;

  position: absolute;
  top: 6rem;
  left: 0;

  width: ${(props) => (props.clicked ? "12rem" : "3.5rem")};
  transition: all 0.5s ease;
  border-radius: 0 30px 30px 0;
`;

const Item = styled(NavLink)`
  text-decoration: none;
  color: var(--white);
  width: 100%;
  padding: 1rem 0;
  cursor: pointer;

  display: flex;
  padding-left: 1rem;

  &:hover {
    border-right: 4px solid var(--white);

    img {
      filter: invert(100%) sepia(0%) saturate(0%) hue-rotate(93deg)
        brightness(103%) contrast(103%);
    }
  }

  img {
    width: 1.2rem;
    height: auto;
    filter: invert(92%) sepia(4%) saturate(1033%) hue-rotate(169deg)
      brightness(78%) contrast(85%);
  }
`;



const Text = styled.span`
  width: ${(props) => (props.clicked ? "100%" : "0")};
  overflow: hidden;
  margin-left: ${(props) => (props.clicked ? "1.5rem" : "0")};
  transition: all 0.3s ease;
`;

const Profile = styled.div`
  width: ${(props) => (props.clicked ? "14rem" : "3rem")};
  height: 3rem;

  padding: 0.5rem 0rem;
  border: 1.7px solid var(--white);
  border-radius: 25px;

  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: ${(props) => (props.clicked ? "11rem" : "0")};

  background-color: var(--black);
  color: var(--white);

  transition: all 0.6s ease;

  img {
    width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    cursor: pointer;
    

    &:hover {
      border: 2px solid var(--grey);
      padding: 2px;
    }
  }
`;

const Details = styled.div`
  display: ${(props) => (props.clicked ? "flex" : "none")};
  justify-content: space-between;
  align-items: center;
  
`;

const Name = styled.div`
  padding: 0 1.5rem;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;


  h4 {
    display: inline-block;
    font-size: 0.9rem;
  }

  a {
    font-size: 0.7rem;
    text-decoration: none;
    color: var(--grey);

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Logout = styled.button`
  border: none;
  width: 2rem;
  height: 2rem;
  background-color: transparent;

  img {
    width: 100%;
    height: auto;
    filter: invert(15%) sepia(70%) saturate(6573%) hue-rotate(2deg)
      brightness(100%) contrast(126%);
    transition: all 0.3s ease;
    &:hover {
      border: none;
      padding: 0;
      opacity: 0.5;
    }
  }
`;
const ProfileName = styled.span`
  background: #512DA8;
  font-size: 25px;
  color: #fff;
  text-align: center;
  line-height: 45px;

  width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    cursor: pointer;
    

    &:hover {
    }
`;
const Sidebar = ({ loginButton, isLoggedIn, toggleModal }) => {

  //console.log("isLoggedIn=",isLoggedIn);
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  let showSide = isLoggedIn == 'LOGOUT' ? true : false;
  //console.log("showSide=",showSide);
  const [showsidebar, setShowsidebar] = useState(showSide);
  const [pic, setPic] = useState('');
  const [profileClick, setprofileClick] = useState(false);
  const handleProfileClick = () => {
    setprofileClick(!profileClick);

  }
  //console.log("showsidebar=",showsidebar);
  React.useEffect(() => {
    setShowsidebar(isLoggedIn == 'LOGOUT' ? true : false);
  }, [isLoggedIn])
  function logoutUserFromApp() {
    logoutUser(localStorage.getItem("userId")).then(resp => {
      // console.log(resp);
    });
    localStorage.removeItem("username");
    localStorage.removeItem("firstname");
    localStorage.removeItem("lastname");
    localStorage.removeItem("email");
    localStorage.removeItem("userId");
    localStorage.removeItem("role");
    localStorage.removeItem("pic");
    localStorage.removeItem("token");
    loginButton("LOGIN");
    window.location.replace("/");
    toggleModal();
  }


  return (
    <>
      {(showsidebar) ? (
        <Container>
          <Button clicked={click} onClick={() => handleClick()}>

          </Button>
          <SidebarContainer>


            <Logo>
              <img src={logo} alt="logo" />
            </Logo>
            <SlickBar clicked={click}>
              <Item
                onClick={() => setClick(false)}
                exact
                activeClassName="active"
                to="/customers"
              >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                  <path fill-rule="evenodd" d="M2.25 2.25a.75.75 0 000 1.5H3v10.5a3 3 0 003 3h1.21l-1.172 3.513a.75.75 0 001.424.474l.329-.987h8.418l.33.987a.75.75 0 001.422-.474l-1.17-3.513H18a3 3 0 003-3V3.75h.75a.75.75 0 000-1.5H2.25zm6.54 15h6.42l.5 1.5H8.29l.5-1.5zm8.085-8.995a.75.75 0 10-.75-1.299 12.81 12.81 0 00-3.558 3.05L11.03 8.47a.75.75 0 00-1.06 0l-3 3a.75.75 0 101.06 1.06l2.47-2.47 1.617 1.618a.75.75 0 001.146-.102 11.312 11.312 0 013.612-3.321z" clip-rule="evenodd" />
                </svg>


                <Text clicked={click}>Customers</Text>
              </Item>

              <Item
                onClick={() => setClick(false)}
                activeClassName="active"
                to="/contracts"
              >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                  <path fill-rule="evenodd" d="M8.603 3.799A4.49 4.49 0 0112 2.25c1.357 0 2.573.6 3.397 1.549a4.49 4.49 0 013.498 1.307 4.491 4.491 0 011.307 3.497A4.49 4.49 0 0121.75 12a4.49 4.49 0 01-1.549 3.397 4.491 4.491 0 01-1.307 3.497 4.491 4.491 0 01-3.497 1.307A4.49 4.49 0 0112 21.75a4.49 4.49 0 01-3.397-1.549 4.49 4.49 0 01-3.498-1.306 4.491 4.491 0 01-1.307-3.498A4.49 4.49 0 012.25 12c0-1.357.6-2.573 1.549-3.397a4.49 4.49 0 011.307-3.497 4.49 4.49 0 013.497-1.307zm7.007 6.387a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
                </svg>




                <Text clicked={click}>Contracts</Text>
              </Item>

              <Item
                onClick={() => setClick(false)}
                activeClassName="active"
                to="/users"
              >
                {localStorage.getItem("role") === "ADMIN" ? (
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                    <path d="M6.25 6.375a4.125 4.125 0 118.25 0 4.125 4.125 0 01-8.25 0zM3.25 19.125a7.125 7.125 0 0114.25 0v.003l-.001.119a.75.75 0 01-.363.63 13.067 13.067 0 01-6.761 1.873c-2.472 0-4.786-.684-6.76-1.873a.75.75 0 01-.364-.63l-.001-.122zM19.75 7.5a.75.75 0 00-1.5 0v2.25H16a.75.75 0 000 1.5h2.25v2.25a.75.75 0 001.5 0v-2.25H22a.75.75 0 000-1.5h-2.25V7.5z" />
                  </svg>) : (
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                    <path fill-rule="evenodd" d="M7.5 6a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zM3.751 20.105a8.25 8.25 0 0116.498 0 .75.75 0 01-.437.695A18.683 18.683 0 0112 22.5c-2.786 0-5.433-.608-7.812-1.7a.75.75 0 01-.437-.695z" clip-rule="evenodd" />
                  </svg>

                )}





                <Text clicked={click}>{localStorage.getItem("role") === "ADMIN" ? "Users" : "Profile"}</Text>
              </Item>

            </SlickBar>

            {
              (localStorage.getItem("email") !== undefined && localStorage.getItem("email") !== "" && localStorage.getItem("email") !== "undefined"
                && localStorage.getItem("email") !== null) ? (

                <Profile clicked={profileClick}>

                  {
                    (pic) ? <img
                      onClick={() => handleProfileClick()}
                      src={pic}
                      alt="Profile"

                    /> :
                      <ProfileName onClick={() => handleProfileClick()}>{localStorage.getItem("firstname").charAt(0).toUpperCase()}{localStorage.getItem("lastname").charAt(0).toUpperCase()}</ProfileName>

                  }

                  <Details clicked={profileClick}>
                    <Name>
                      <h4>{localStorage.getItem("firstname")}&nbsp;{localStorage.getItem("lastname")}</h4>

                      {/* <NavLink to="/profile">view&nbsp;profile</NavLink> */}
                    </Name>


                    <Logout>
                      <img src={PowerOff} onClick={logoutUserFromApp} alt="logout" />
                    </Logout>
                  </Details>
                </Profile>
              ) : ""}
          </SidebarContainer>
        </Container>
      ) : ""}
    </>
  );
};

export default Sidebar;
