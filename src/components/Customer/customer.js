import SearchIcon from "@mui/icons-material/Search";
import { FormControl, Grid, InputAdornment, Snackbar, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Tooltip } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import add from '../../assets/add.png';
import contractors from '../../assets/contactors.png';
import deletes from '../../assets/delete.png';
import edit from '../../assets/edit.png';
import users from '../../assets/users.png';
import { deleteCustomers, getCustomerByUserIdData, getCustomerData } from '../../services/services';
import AddCustomer from './addCustomer';
import EditCustomer from './editCustomer';
import ViewContracts from './viewContracts';
import ViewUsers from './viewUsers';
export default function Home() {

  const [logButtonName, setlogButtonName] = React.useState((localStorage.getItem("email") === "" && localStorage.getItem("email") === undefined
    && localStorage.getItem("email") === null) ? "LOGIN" : "LOGOUT");
  const [isOpen, setIsOpen] = React.useState(false);
  const [value, setValue] = React.useState(1);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackMessage, setSnackMessage] = React.useState('');
  const [customerData, setCustomerData] = React.useState([]);
  const [isAddCustomerOpen, setIsAddCustomerOpen] = React.useState(false);
  const [isEditCustomerOpen, setIsEditCustomerOpen] = React.useState(false);
  const [isViewContractOpen, setIsViewContractOpen] = React.useState(false);
  const [isViewCustomerOpen, setIsViewCustomerOpen] = React.useState(false);
  const [eCustomerId, seteCustomerId] = React.useState('');
  const [eCustomerName, seteCustomerName] = React.useState('');
  const [eDepartment, seteDepartment] = React.useState("");
  const [eAddress, seteAddress] = React.useState("");


  function toggleModal() {
    if (logButtonName === 'LOGOUT') {
      setIsOpen(!isOpen);
    } else {
      setIsOpen(!isOpen);
    }
  }

  React.useEffect(() => {

    if (localStorage.getItem("role") !== "" && localStorage.getItem("role") !== undefined
      && localStorage.getItem("role") === "ADMIN") {
      getCustomerData("").then(resp => {
        console.log(resp.data);
        setCustomerData(resp.data);
      }).catch(error => {
        console.log("login user err ", error);
      });
    } else {
      getCustomerByUserIdData().then(resp => {
        console.log(resp.data);
        let arr = [];
        arr.push(resp.data);
        setCustomerData(arr);
      }).catch(error => {
        console.log("login user err ", error);
      });
    }

  }, []);

  const loginHandler = (value) => {
    setIsLoggedIn(value);
  }
  React.useEffect(() => {
    getLoggedInStatus();

  }, [value]);


  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };

  function getLoggedInStatus() {
    if (localStorage.getItem("email") !== "" && localStorage.getItem("email") !== undefined
      && localStorage.getItem("email") !== null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }
  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1000px !important',
      height: '800px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogForViewMovie = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1200px !important',
      height: '900px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));
  const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  const DialogAddSlot = styled(Dialog)(({ theme }) => ({
    '& .MuiDialog-paper': {
      padding: theme.spacing(2),
      minWidth: '1500px !important',
      height: '800px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

  const DialogTitleForModal = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };


  DialogTitleForModal.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

  function toggleAddCustomerModal() {
    setIsAddCustomerOpen(!isAddCustomerOpen);
    if (isAddCustomerOpen === true) {
      getCustomerData("").then(resp => {
        console.log(resp.data);
        setCustomerData(resp.data);

      }).catch(error => {
        console.log("login user err " + error);
      });
    }
  }


  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const columns = [
    { id: 'id', label: 'CUSTOMER ID', maxWidth: 100 },
    { id: 'name', label: 'CUSTOMER  NAME', maxWidth: 100 },
    { id: 'department', label: 'DEPARTMENT', maxWidth: 30 },
    { id: 'address', label: 'ADDRESS', maxWidth: 100 },
    { id: 'action', label: 'ACTION', maxWidth: 100 }
  ];


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const editCustomer = (id, pos) => {
    seteCustomerId(id);
    seteCustomerName(customerData[pos].name);
    seteDepartment(customerData[pos].department);
    seteAddress(customerData[pos].address);
    toggleEditCustomerModal();
  }

  function toggleEditCustomerModal() {
    setIsEditCustomerOpen(!isEditCustomerOpen);
    if (isEditCustomerOpen === true) {
      getCustomerData("").then(resp => {
        console.log(resp.data);
        setCustomerData(resp.data);

      }).catch(error => {
        console.log("login user err " + error);
        setOpenSnack(true);
        setSnackMessage(error.response.data.message);
      });
    }
  }

  const viewContractsById = (id) => {
    seteCustomerId(id);
    toggleViewContractModal();
  }

  const viewUsersById = (id) => {
    seteCustomerId(id);
    toggleViewCustomerModal();
  }

  function toggleViewCustomerModal() {
    setIsViewCustomerOpen(!isViewCustomerOpen);
  }

  function toggleViewContractModal() {
    setIsViewContractOpen(!isViewContractOpen);
  }

  const deleteCustomer = (id) => {
    console.log(id);
    deleteCustomers(id).then(resp => {
      if (resp.status === 500) {
        setSnackMessage('Error occured during delete customer');
        setOpenSnack(true);
      } else {
        console.log(resp);
        setSnackMessage('Customer deleted successfully');
        setOpenSnack(true);


        getCustomerData("").then(resp => {
          console.log(resp.data);
          setCustomerData(resp.data);

        }).catch(error => {
          console.log("login user err " + error);
          setOpenSnack(true);
          setSnackMessage(error.response.data.message);
        });

      }

    }).catch(error => {
      setOpenSnack(true);
      setSnackMessage("Deletion failed: " + error.response.data.message);
      console.log("login user err " + error);
    })
  }

  return (
    <React.Fragment>

      {((localStorage.getItem("role") !== "" && localStorage.getItem("role") !== undefined
        && localStorage.getItem("role") === "ADMIN")) ? (




        <Grid container direction="row" spacing={1}
          style={{ paddingLeft: '100px' }} >
          <Grid md={1}></Grid>
          <Grid md={10}>
            <br></br><br></br><br></br>
            <FormControl fullWidth variant='standard' required={true} style={{ textAlign: 'center', marginLeft: '100px', width: '75%' }}>
              <TextField
                size="small"
                label="Search Customers"

                style={{ backgroundColor: "white", border: '0', borderColor: 'gray !important' }}
                onChange={event => {
                  let val = event.target.value;
                  getCustomerData(val).then(resp => {
                    console.log(resp.data);
                    setCustomerData(resp.data);

                  }).catch(error => {
                    console.log("login user err " + error);
                  })
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment>
                      <IconButton>
                        <SearchIcon />
                      </IconButton>
                    </InputAdornment>
                  )


                }}
              /></FormControl>

            <Image src={add} onClick={toggleAddCustomerModal} style={{ float: 'right', height: '80px', marginRight: '3%', marginTop: '-10px', cursor: 'pointer' }}></Image>
            <br></br><br></br>
            <div className="min-w-screen flex items-center justify-center overflow-hidden rounded-2xl bg-gray-900">

              <div className="w-full ">

                <div className="bg-white shadow-md rounded my-6">
                  <TableContainer sx={{ minHeight: 277, maxHeight: 300 }} class="border-gray-900 hover:bg-gray-600">
                    <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto " >
                      <TableHead>
                        <TableRow class="bg-gray-900 text-white uppercase leading-normal">
                          {columns.map((column) => (
                            <TableCell
                              key={column.id}
                              align="center"
                              style={{ maxWidth: column.maxWidth }}
                              class="py-3 px-6 text-center bg-gray-900"
                            >
                              {column.label}
                            </TableCell>
                          ))}
                        </TableRow>
                      </TableHead>
                      <TableBody class="text-gray-200 bg-gray-700 text-sm font-light  ">
                        {customerData && customerData.length > 0 ? (customerData
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map((row, ind) => {
                            return (
                              <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                                {columns.map((column) => {
                                  const id = row["id"];
                                  const value = row[column.id];
                                  return (
                                    <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                      {/* {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value} */}


                                      {(column.id === 'action') ? (
                                        <>
                                          <Tooltip title="Edit customers">
                                            <IconButton aria-label="edit" onClick={(e) => editCustomer(id, ind)} style={{ color: '#566573' }} size="small">

                                              <Image src={edit} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton>
                                          </Tooltip>&nbsp;
                                          <Tooltip title="Delete customers">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => deleteCustomer(id)} aria-label="delete" size="small">

                                              <Image src={deletes} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>&nbsp;
                                          <Tooltip title="View contracts">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => viewContractsById(id)} aria-label="delete" size="small">

                                              <Image src={contractors} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>&nbsp;
                                          <Tooltip title="View users">
                                            <IconButton style={{ color: 'red' }} onClick={(e) => viewUsersById(id)} aria-label="delete" size="small">

                                              <Image src={users} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                            </IconButton></Tooltip>
                                        </>
                                      ) :
                                        (column.id === 'sNo') ? (
                                          ind + 1
                                        ) : value}
                                    </TableCell>
                                  );
                                })}
                              </TableRow>
                            );
                          })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={customerData.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    class="bg-gray-900 text-white uppercase leading-normal"
                  />
                </div>
              </div>
            </div>
          </Grid>
          <Grid md={1}></Grid>






        </Grid>
      ) : (
        <>
          <br></br><br></br>
          <Grid container direction="row" spacing={1}
            style={{ paddingLeft: '100px' }} >
            <Grid md={1}></Grid>
            <Grid md={10}>
              <br></br><br></br>
              <div className="min-w-screen flex items-center justify-center overflow-hidden rounded-2xl bg-gray-900">

                <div className="w-full ">

                  <div className="bg-white shadow-md rounded my-6">
                    <TableContainer sx={{ minHeight: 277, maxHeight: 300 }} class="border-b border-gray-900 hover:bg-gray-600">
                      <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto " >
                        <TableHead>
                          <TableRow class="bg-gray-900 text-white uppercase leading-normal">
                            {columns.map((column) => (
                              <TableCell
                                key={column.id}
                                align="center"
                                style={{ maxWidth: column.maxWidth }}
                                class="py-3 px-6 text-center bg-gray-900"
                              >
                                {column.label}
                              </TableCell>
                            ))}
                          </TableRow>
                        </TableHead>
                        <TableBody class="text-gray-200 bg-gray-700 text-sm font-light  ">
                          {customerData && customerData.length > 0 ? (customerData
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, ind) => {
                              return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                                  {columns.map((column) => {
                                    const id = row["id"];
                                    const value = row[column.id];
                                    return (
                                      <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                        {/* {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value} */}


                                        {(column.id === 'action') ? (
                                          <>

                                            <Tooltip title="View contracts">
                                              <IconButton style={{ color: 'red' }} onClick={(e) => viewContractsById(id)} aria-label="delete" size="small">

                                                <Image src={contractors} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                              </IconButton></Tooltip>&nbsp;
                                            <Tooltip title="View users">
                                              <IconButton style={{ color: 'red' }} onClick={(e) => viewUsersById(id)} aria-label="delete" size="small">

                                                <Image src={users} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                              </IconButton></Tooltip>
                                          </>
                                        ) :
                                          (column.id === 'sNo') ? (
                                            ind + 1
                                          ) : value}
                                      </TableCell>
                                    );
                                  })}
                                </TableRow>
                              );
                            })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                        </TableBody>
                      </Table>
                    </TableContainer>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25]}
                      component="div"
                      count={customerData.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      onPageChange={handleChangePage}
                      onRowsPerPageChange={handleChangeRowsPerPage}
                      class="bg-gray-900 text-white uppercase leading-normal"
                    />
                  </div>
                </div>
              </div>
            </Grid>
            <Grid md={1}></Grid>
          </Grid>
        </>
      )}


      <DialogAddSlot
        onClose={toggleAddCustomerModal}
        aria-labelledby="customized-dialog-title"
        open={isAddCustomerOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          ADD NEW CUSTOMER
        </DialogTitleForModal>

        <AddCustomer addCustomerModal={toggleAddCustomerModal} />

      </DialogAddSlot>

      <DialogAddSlot
        onClose={toggleEditCustomerModal}
        aria-labelledby="customized-dialog-title"
        open={isEditCustomerOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          EDIT CUSTOMER
        </DialogTitleForModal>

        <EditCustomer editCustomerModal={toggleEditCustomerModal} ecustomerId={eCustomerId} ename={eCustomerName} edepartment={eDepartment} eaddress={eAddress} />

      </DialogAddSlot>

      <DialogAddSlot
        onClose={toggleViewContractModal}
        aria-labelledby="customized-dialog-title"
        open={isViewContractOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          VIEW CONTRACTS
        </DialogTitleForModal>

        <ViewContracts viewContractsModel={toggleViewContractModal} customerId={eCustomerId} />

      </DialogAddSlot>

      <DialogAddSlot
        onClose={toggleViewCustomerModal}
        aria-labelledby="customized-dialog-title"
        open={isViewCustomerOpen}
      >
        <DialogTitleForModal id="customized-dialog-title" className="toolHeader" style={{ textAlign: 'center', color: 'black' }}>
          VIEW USERS
        </DialogTitleForModal>

        <ViewUsers viewUsersModel={toggleViewCustomerModal} customerId={eCustomerId} />

      </DialogAddSlot>

      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}