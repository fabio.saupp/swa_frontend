import { FormControl, Grid, Snackbar, TextField } from '@mui/material';
import Button from '@mui/material/Button';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import customer from '../../assets/customer.svg';
import { editCustomer } from '../../services/services';
export default function EditCustomer({ editCustomerModal, ecustomerId, ename, edepartment, eaddress }) {
  console.log(ecustomerId, ename, edepartment, eaddress);
  const [customerName, setCustomerName] = React.useState(ename);
  const [department, setDepartment] = React.useState(edepartment);
  const [address, setAddress] = React.useState(eaddress);
  const [snackMessage, setSnackMessage] = React.useState('');
  const [openSnack, setOpenSnack] = React.useState(false);
  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };
  const handleCustomerDeptChange = (e) => {
    setDepartment(e.target.value);
  };
  const handleCustomerNameChange = (e) => {
    setCustomerName(e.target.value);
  };
  const handleCustomerAddressChange = (e) => {
    setAddress(e.target.value);
  };



  function editIdCustomer() {

    console.log(ecustomerId, customerName, department, address);
    if (customerName === "" || customerName === undefined || department === "" || department === undefined ||
      address === "" || address === undefined) {
      setSnackMessage('Fields cannot be blank');
      setOpenSnack(true);
    } else {

      editCustomer(ecustomerId, customerName, department, address).then(resp => {
        console.log(resp);
        let data = resp.data;
        console.log(data);
        setSnackMessage('Customer updated successfully');
        setOpenSnack(true);
        editCustomerModal();
      });
    }
  }
  return (
    <React.Fragment>
      <DialogContent>
        <Grid container >
          <Grid item xs={5}>
            <Image src={customer} style={{ marginLeft: '20px' }} />

          </Grid>
          <Grid item xs={1}></Grid>
          <Grid item xs={6}>

            <br></br><br></br>
            <FormControl required={true} fullWidth variant="standard" style={{ textAlign: 'center' }}>
              <TextField
                id="standard-adornment-fname"
                label="Enter Customer Name"
                size="small"
                type={'text'}
                value={customerName}
                onChange={handleCustomerNameChange}
              />
            </FormControl>
            <br></br><br></br>
            <FormControl required={true} fullWidth variant="standard" style={{ textAlign: 'center' }}>
              <TextField
                id="standard-adornment-fname"
                label="Department"
                size="small"
                type={'text'}
                value={department}
                onChange={handleCustomerDeptChange}
              />
            </FormControl>

            <br></br><br></br>

            <FormControl required={true} fullWidth variant="standard" style={{ textAlign: 'center' }}>
              <TextField
                id="standard-adornment-fname"
                size="small"
                label="Address"
                type={'text'}
                value={address}
                onChange={handleCustomerAddressChange}
              />
            </FormControl>


          </Grid>





        </Grid>
      </DialogContent>
      <DialogActions align='center'>
        <Button variant="contained" style={{ backgroundColor: "#566573" }} onClick={editIdCustomer}>&nbsp;UPDATE CUSTOMER</Button>
      </DialogActions>
      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}