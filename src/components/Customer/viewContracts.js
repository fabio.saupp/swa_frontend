import { Button, DialogActions, DialogContent, Grid, Snackbar, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@mui/material';
import * as React from 'react';
import { getAllContractsByCustomerId } from '../../services/services';
export default function ViewContracts({ viewContractsModel, customerId }) {

  const [logButtonName, setlogButtonName] = React.useState((localStorage.getItem("email") === "" && localStorage.getItem("email") === undefined
    && localStorage.getItem("email") === null) ? "LOGIN" : "LOGOUT");
  const [isOpen, setIsOpen] = React.useState(false);
  const [value, setValue] = React.useState(1);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackMessage, setSnackMessage] = React.useState('');
  const [ContractData, setContractData] = React.useState([]);



  function toggleModal() {
    if (logButtonName === 'LOGOUT') {
      setIsOpen(!isOpen);
    } else {
      setIsOpen(!isOpen);
    }
  }

  React.useEffect(() => {
    getAllContractsByCustomerId(customerId).then(resp => {
      console.log(JSON.stringify(resp));
      console.log(resp.data);
      setContractData(resp.data);
    }).catch(error => {
      console.log("login Contract err ", error);
    });

  }, []);

  const loginHandler = (value) => {
    setIsLoggedIn(value);
  }
  React.useEffect(() => {
    getLoggedInStatus();

  }, [value]);


  const handleSnackClose = () => {
    setOpenSnack(!openSnack);
  };

  function getLoggedInStatus() {
    if (localStorage.getItem("email") !== "" && localStorage.getItem("email") !== undefined
      && localStorage.getItem("email") !== null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const columns = [
    { id: 'startDate', label: 'START DATE', maxWidth: 100 },
    { id: 'endDate', label: 'END DATE', maxWidth: 100 },
    { id: 'version', label: 'VERSION', maxWidth: 100 },

  ];


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <React.Fragment>
      <DialogContent>
        <Grid container direction="row" spacing={1}
          style={{ paddingLeft: '100px' }} >

          <Grid md={12}>
            <br></br><br></br><br></br>
            <>
              <TableContainer class="rounded">
                <Table stickyHeader aria-label="sticky table" class="min-w-max w-full table-auto rounded" >
                  <TableHead>
                    <TableRow class="bg-gray-900 text-white uppercase leading-normal rounded">
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align="center"
                          style={{ maxWidth: column.maxWidth }}
                          class="py-3 px-6 text-center bg-gray-900"
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody class="text-gray-200 bg-gray-700 text-sm font-light">
                    {ContractData && ContractData.length > 0 ? (ContractData
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row, ind) => {
                        return (
                          <TableRow hover role="checkbox" tabIndex={-1} key={row.code} class="border-b border-gray-900 hover:bg-gray-600 ">
                            {columns.map((column) => {
                              const id = row["id"];
                              const value = row[column.id];
                              return (
                                <TableCell key={column.id} align={column.align} class="py-3 px-6 text-left whitespace-nowrap text-center">
                                  {/* {column.format && typeof value === 'number'
                        ? column.format(value)
                        : value} */}


                                  {(column.id === 'action') ? (
                                    <>
                                      {/* <Tooltip title="Edit user">
                                                <IconButton aria-label="edit" onClick={(e) => editUser(id, ind)} style={{ color: '#566573' }} size="small">

                                                  <Image src={edit} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                                </IconButton>
                                              </Tooltip>&nbsp;
                                              <Tooltip title="Delete user">
                                                <IconButton style={{ color: 'red' }} onClick={(e) => deleteUser(id)} aria-label="delete" size="small">

                                                  <Image src={deletes} style={{ float: 'right', height: '30px', cursor: 'pointer' }}></Image>
                                                </IconButton></Tooltip>&nbsp; */}
                                    </>
                                  ) :
                                    (column.id === 'sNo') ? (
                                      ind + 1
                                    ) : value}
                                </TableCell>
                              );
                            })}
                          </TableRow>
                        );
                      })) : <TableRow><TableCell colSpan={9} style={{ textAlign: 'center', color: 'red' }}>No Records found</TableCell></TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={ContractData.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                class="bg-gray-900 text-white uppercase leading-normal"
              />
            </>
            <br></br><br></br><br></br><br></br>
          </Grid>






        </Grid>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" style={{ backgroundColor: "#566573", float: 'right' }} onClick={viewContractsModel}>&nbsp;CLOSE</Button>
      </DialogActions>


      <Snackbar
        style={{ whiteSpace: 'pre-wrap', width: '300px', top: '50%', bottom: '50%', left: '40%', right: '50%' }}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        open={openSnack}
        onClose={handleSnackClose}
        message={snackMessage}
      />
    </React.Fragment>
  );
}