import { AnimatePresence } from "framer-motion";
import React from 'react';
import 'react-loading-skeleton/dist/skeleton.css';
import { Route, Switch, useLocation } from "react-router";
import styled from "styled-components";
import Contractor from "./components/Contractor/contractor";
import Home from "./components/Customer/customer";
import Navbar from "./components/Navbar/Navbar";
import Users from "./components/Users/users";

const Pages = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
 
  

  h1 {
    font-size: calc(2rem + 2vw);
    background: linear-gradient(to right, #803bec 30%, #1b1b1b 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;

  }
`;

function App() {
  const location = useLocation();

  const [logButtonName, setlogButtonName] = React.useState((localStorage.getItem("email") === "" && localStorage.getItem("email") === undefined
    && localStorage.getItem("email") === null) ? "LOGIN" : "LOGOUT");
  return (
    <>



      <Navbar logButtonName={logButtonName} setlogButtonName={setlogButtonName} />
      <div class="overflow-y-auto">
        <Pages >
          <AnimatePresence exitBeforeEnter >
            <Switch location={location} key={location.pathname}>

              <Route exact path="/"
                render={(props) => <Home isLoggedIn={logButtonName} />} />
              <Route exact path="/customers"
                render={(props) => <Home isLoggedIn={logButtonName} />} />
              <Route exact path="/contracts"
                render={(props) => <Contractor isLoggedIn={logButtonName} />} />
              <Route exact path="/users"
                render={(props) => <Users isLoggedIn={logButtonName} />} />
            </Switch>
          </AnimatePresence>
        </Pages>
      </div>

    </>
  );
}

export default App;
